#!/bin/bash

unset GOROOT
export GOPATH=${PWD}

mkdir bin 2> /dev/null
cd src

go get github.com/gorilla/mux
go build -ldflags="-s -w" -o ../bin/portscanner
