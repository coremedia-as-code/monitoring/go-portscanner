# CoreMedia Monitoring - Portscanner

## build

you can use the included `build.sh` or

## usage

```
$ curl localhost:8088


 Welcome!
 this is a simple port scanner for CoreMedia Applications

 Try this:
   curl -v -H "Content-Type: application/json" localhost:8088/scan/${HOSTNAME}
 or this
   curl -v -H "Content-Type: application/json" localhost:8088/scan/${HOSTNAME} --data '{ "ports": [22,80,3306,999,55555] }'
```
